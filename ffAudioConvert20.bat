@ECHO off
REM Please remember that the code won't work if you put a backslash at the end of the dir variables
REM Where is the Source?
set sourceDir=Source\20
REM Where is the PAL SpeedUp Source?
set sourceDirPAL=Source\20PAL
REM Where is the destination?
set outputDir=Output
REM Where did you put ffmpeg? (I used . because I have it in the same directory as the batch file)
set ffDir=.
REM These are the bitrate variables, in case you want to change them
set m4aBit=76k
set usbBit=96k
set mp3Bit=128k
set ac3Bit=192k
set SMBXbit=128k
REM Actual code, yay!
:start
ECHO k4 - Convert Stereo to m4a
ECHO p4 - Convert PAL Stereo to m4a
ECHO p41 - Timestretch PAL Stereo to m4a
ECHO b4 - Convert Stereo + PAL Stereo to m4a
ECHO k3 - Convert Stereo to mp3
ECHO p3 - Convert PAL Stereo to mp3
ECHO b3 - Convert Stereo + PAL Stereo to mp3
ECHO ak3 - Convert Stereo to Stereo ac3
ECHO ap3 - Convert PAL Stereo to Stereo ac3
ECHO k7 - Convert Stereo to m4a + mp3
ECHO p7 - Convert PAL Stereo to m4a + mp3
ECHO p4a - Convert PAL Stereo to %mp3Bit% m4a
ECHO p4b - Timestretch PAL Stereo to %mp3Bit% m4a
ECHO uk - Convert Stereo to usb (%usbBit%) mp3
ECHO up - Convert PAL Stereo to usb (%usbBit%) mp3
ECHO wak - Convert Stereo to wav
ECHO lua - Convert to ogg for use in SMBX
ECHO l0 - Delete logfiles
ECHO. 
set kop=
set /p kop=Choose: 
if '%kop%'=='k4' goto keep4
if '%kop%'=='p4' goto pal4
if '%kop%'=='p41' goto pal41
if '%kop%'=='b4' goto both4
if '%kop%'=='k3' goto keep3
if '%kop%'=='p3' goto pal3
if '%kop%'=='b3' goto both3
if '%kop%'=='ak3' goto ak3
if '%kop%'=='ap3' goto ap3
if '%kop%'=='k7' goto keep7
if '%kop%'=='p7' goto pal7
if '%kop%'=='p4a' goto pal4a
if '%kop%'=='p4b' goto pal4b
if '%kop%'=='uk' goto usbk
if '%kop%'=='up' goto usbp
if '%kop%'=='wak' goto wavk
if '%kop%'=='lua' goto lua
if '%kop%'=='l0' goto log0
if '%kop%'=='x' goto end
ECHO.
ECHO "%kop%" is not valid, try again.
ECHO.
goto start
:keep4
FOR %%A IN ("%sourceDir%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -b:a %m4aBit% "%outputDir%\%%~nA.m4a"
goto end
:pal4
FOR %%A IN ("%sourceDirPAL%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -af asetpts='25000/23976*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3 -b:a %m4aBit% "%outputDir%\%%~nA.m4a"
goto end
:both4
FOR %%A IN ("%sourceDir%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -b:a %m4aBit% "%outputDir%\%%~nA.m4a"
FOR %%A IN ("%sourceDirPAL%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -af asetpts='25000/23976*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3 -b:a %m4aBit% "%outputDir%\%%~nA.m4a"
goto end
:keep3
FOR %%A IN ("%sourceDir%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -b:a %mp3Bit% "%outputDir%\%%~nA.mp3"
goto end
:pal3
FOR %%A IN ("%sourceDirPAL%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -af asetpts='25000/23976*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3 -b:a %mp3Bit% "%outputDir%\%%~nA.mp3"
goto end
:both3
FOR %%A IN ("%sourceDir%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -b:a %mp3Bit% "%outputDir%\%%~nA.mp3"
FOR %%A IN ("%sourceDirPAL%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -af asetpts='25000/23976*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3 -b:a %mp3Bit% "%outputDir%\%%~nA.mp3"
goto end
:ak3
FOR %%A IN ("%sourceDir%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -b:a %ac3Bit% "%outputDir%\%%~nA.ac3"
:ap3
FOR %%A IN ("%sourceDirPAL%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -af asetpts='25000/23976*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3 -b:a %ac3Bit% "%outputDir%\%%~nA.ac3"
goto end
:keep7
FOR %%A IN ("%sourceDir%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -b:a %m4aBit% "%outputDir%\%%~nA.m4a"
FOR %%A IN ("%sourceDir%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -b:a %mp3Bit% "%outputDir%\%%~nA.mp3"
goto end
:pal7
FOR %%A IN ("%sourceDirPAL%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -af asetpts='25000/23976*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3 -b:a %m4aBit% "%outputDir%\%%~nA.m4a"
FOR %%A IN ("%sourceDirPAL%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -af asetpts='25000/23976*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3 -b:a %mp3Bit% "%outputDir%\%%~nA.mp3"
:log0
del *.log
goto end
:pal4a
FOR %%A IN ("%sourceDirPAL%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -af asetpts='25000/23976*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3 -b:a %mp3Bit% "%outputDir%\%%~nA.m4a"
goto end
:pal4b
FOR %%A IN ("%sourceDirPAL%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -af "atempo=0.95904" -b:a %mp3Bit% "%outputDir%\%%~nA.m4a"
goto end
:pal41
FOR %%A IN ("%sourceDirPAL%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -af "atempo=0.95904" -b:a %m4aBit% "%outputDir%\%%~nA.m4a"
goto end
:usbk
FOR %%A IN ("%sourceDir%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -b:a %usbBit% "%outputDir%\%%~nA.mp3"
goto end
:usbp
FOR %%A IN ("%sourceDirPAL%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -af asetpts='25000/23976*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3 -b:a %usbBit% "%outputDir%\%%~nA.mp3"
goto end
:wavk
FOR %%A IN ("%sourceDir%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" "%outputDir%\%%~nA.wav"
goto end
:lua
FOR %%A IN ("%sourceDir%\*") DO "%ffDir%\ffmpeg.exe" -report -i "%%~A" -c libvorbis -b:a %SMBXBit% "%outputDir%\%%~nA.ogg"
goto end
:end
ECHO.
ECHO All done!
ECHO.