@ECHO off
REM Remember that the code won't work if you put a backslash at the end of the dir variables
REM Where is the Source?
set sourceDir=Source\51
REM Where is the PAL SpeedUp Source?
set sourceDirPAL=Source\51PAL
REM Where is the destination?
set outputDir=Output
REM Where did you put ffmpeg? (I used . because I have it in the same directory as the batch file)
set ffDir=.
REM These are the bitrate variables, in case you want to change them
set m4aBit=76k
set ac3Bit=192k
REM Actual code, yay!
:start
ECHO k4 - Convert 5.1 ac3 to m4a Stereo F
ECHO p4 - Convert PAL 5.1 ac3 to m4a Stereo F
ECHO b4 - Convert 5.1 ac3 + PAL 5.1 ac3 to m4a Stereo F
ECHO k3 - Convert 5.1 ac3 to ac3 Stereo F
ECHO p3 - Convert PAL 5.1 ac3 to ac3 Stereo F
ECHO b3 - Convert 5.1 ac3 + PAL 5.1 ac3 to ac3 Stereo F
ECHO k7 - Convert 5.1 ac3 to m4a Stereo + ac3 Stereo F
ECHO p7 - Convert PAL 5.1 ac3 to m4a Stereo + ac3 Stereo F
ECHO s4 - Convert 5.1 ac3 to PAL m4a Stereo F
ECHO s3 - Convert 5.1 ac3 to PAL ac3 Stereo F
ECHO s7 - Convert 5.1 ac3 to PAL m4a Stereo + ac3 Stereo F
ECHO d4 - Convert 5.1 DTS to m4a Stereo B
ECHO d3 - Convert 5.1 DTS to ac3 Stereo B
ECHO d7 - Convert 5.1 DTS to m4a Stereo + ac3 Stereo B
ECHO l0 - Delete logfiles
ECHO. 
set kop=
set /p kop=Choose: 
if '%kop%'=='k4' goto keep4
if '%kop%'=='p4' goto pal4
if '%kop%'=='b4' goto both4
if '%kop%'=='k3' goto keep3
if '%kop%'=='p3' goto pal3
if '%kop%'=='b3' goto both3
if '%kop%'=='k7' goto keep7
if '%kop%'=='p7' goto pal7
if '%kop%'=='d4' goto dk4
if '%kop%'=='d3' goto dk3
if '%kop%'=='d7' goto dk7
if '%kop%'=='s4' goto ntsc4
if '%kop%'=='s3' goto ntsc3
if '%kop%'=='s7' goto ntsc7
if '%kop%'=='l0' goto log0
if '%kop%'=='x' goto end
ECHO.
ECHO "%kop%" is not valid, try again.
ECHO.
goto start
:keep4
FOR %%A IN ("%sourceDir%\*") DO "ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -af "pan=stereo|FL=1.369398*FC+1.261204*FL+0.815301*BL|FR=1.369398*FC+1.261204*FR+0.815301*BR" -b:a %m4aBit% "%outputDir%\%%~nA.m4a"
goto end
:pal4
FOR %%A IN ("%sourceDirPAL%\*") DO "ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -af "pan=stereo|FL=1.369398*FC+1.261204*FL+0.815301*BL|FR=1.369398*FC+1.261204*FR+0.815301*BR,asetpts='25000/23976*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3" -b:a %m4aBit% "%outputDir%\%%~nA.m4a"
goto end
:ntsc4
FOR %%A IN ("%sourceDir%*") DO "ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -af "pan=stereo|FL=1.369398*FC+1.261204*FL+0.815301*BL|FR=1.369398*FC+1.261204*FR+0.815301*BR,asetpts='23976/25000*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3" -b:a %m4aBit% "%outputDir%\%%~nA.m4a"
goto end
:both4
FOR %%A IN ("%sourceDir%\*") DO "ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -af "pan=stereo|FL=1.369398*FC+1.261204*FL+0.815301*BL|FR=1.369398*FC+1.261204*FR+0.815301*BR" -b:a %m4aBit% "%outputDir%\%%~nA.m4a"
FOR %%A IN ("%sourceDirPAL%\*") DO "ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -af "pan=stereo|FL=1.369398*FC+1.261204*FL+0.815301*BL|FR=1.369398*FC+1.261204*FR+0.815301*BR,asetpts='25000/23976*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3" -b:a %m4aBit% "%outputDir%\%%~nA.m4a"
goto end
:keep3
FOR %%A IN ("%sourceDir%\*") DO "ffmpeg.exe" -report -i "%%~A" -af "pan=stereo|FL=1.369398*FC+1.261204*FL+0.815301*BL|FR=1.369398*FC+1.261204*FR+0.815301*BR" -b:a %ac3Bit% "%outputDir%\%%~nA.ac3"
goto end
:pal3
FOR %%A IN ("%sourceDirPAL%\*") DO "ffmpeg.exe" -report -i "%%~A" -af "pan=stereo|FL=1.369398*FC+1.261204*FL+0.815301*BL|FR=1.369398*FC+1.261204*FR+0.815301*BR,asetpts='25000/23976*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3" -b:a %ac3Bit% "%outputDir%\%%~nA.ac3"
goto end
:ntsc3
FOR %%A IN ("%sourceDir%\*") DO "ffmpeg.exe" -report -i "%%~A" -af "pan=stereo|FL=1.369398*FC+1.261204*FL+0.815301*BL|FR=1.369398*FC+1.261204*FR+0.815301*BR,asetpts='23976/25000*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3" -b:a %ac3Bit% "%outputDir%\%%~nA.ac3"
goto end
:both3
FOR %%A IN ("%sourceDir%\*") DO "ffmpeg.exe" -report -i "%%~A" -af "pan=stereo|FL=1.369398*FC+1.261204*FL+0.815301*BL|FR=1.369398*FC+1.261204*FR+0.815301*BR" -b:a %ac3Bit% "%outputDir%\%%~nA.ac3"
FOR %%A IN ("%sourceDirPAL%\*") DO "ffmpeg.exe" -report -i "%%~A" -af "pan=stereo|FL=1.369398*FC+1.261204*FL+0.815301*BL|FR=1.369398*FC+1.261204*FR+0.815301*BR,asetpts='25000/23976*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3" -b:a %ac3Bit% "%outputDir%\%%~nA.ac3"
goto end
:keep7
FOR %%A IN ("%sourceDir%\*") DO "ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -af "pan=stereo|FL=1.369398*FC+1.261204*FL+0.815301*BL|FR=1.369398*FC+1.261204*FR+0.815301*BR" -b:a %m4aBit% "%outputDir%\%%~nA.m4a"
FOR %%A IN ("%sourceDir%\*") DO "ffmpeg.exe" -report -i "%%~A" -af "pan=stereo|FL=1.369398*FC+1.261204*FL+0.815301*BL|FR=1.369398*FC+1.261204*FR+0.815301*BR" -b:a %ac3Bit% "%outputDir%\%%~nA.ac3"
goto end
:pal7
FOR %%A IN ("%sourceDirPAL%\*") DO "ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -af "pan=stereo|FL=1.369398*FC+1.261204*FL+0.815301*BL|FR=1.369398*FC+1.261204*FR+0.815301*BR,asetpts='25000/23976*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3" -b:a %m4aBit% "%outputDir%\%%~nA.m4a"
FOR %%A IN ("%sourceDirPAL%\*") DO "ffmpeg.exe" -report -i "%%~A" -af "pan=stereo|FL=1.369398*FC+1.261204*FL+0.815301*BL|FR=1.369398*FC+1.261204*FR+0.815301*BR,asetpts='25000/23976*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3" -b:a %ac3Bit% "%outputDir%\%%~nA.ac3"
goto end
:ntsc7
FOR %%A IN ("%sourceDir%\*") DO "ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -af "pan=stereo|FL=1.369398*FC+1.261204*FL+0.815301*BL|FR=1.369398*FC+1.261204*FR+0.815301*BR,asetpts='23976/25000*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3" -b:a %m4aBit% "%outputDir%\%%~nA.m4a"
FOR %%A IN ("%sourceDir%\*") DO "ffmpeg.exe" -report -i "%%~A" -af "pan=stereo|FL=1.369398*FC+1.261204*FL+0.815301*BL|FR=1.369398*FC+1.261204*FR+0.815301*BR,asetpts='23976/25000*(PTS-STARTPTS)',aresample=none:async=1:min_comp=0.01:comp_duration=1:max_soft_comp=100000000:min_hard_comp=0.3" -b:a %ac3Bit% "%outputDir%\%%~nA.ac3"
goto end
:dk4
FOR %%A IN ("%sourceDir%\*.dts") DO "ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -af "pan=stereo|FL=1.369398*FC+1.261204*BL+0.815301*FL|FR=1.369398*FC+1.261204*BR+0.815301*FR" -b:a %m4aBit% "%outputDir%\%%~nA.m4a"
goto end
:dk3
FOR %%A IN ("%sourceDir%\*.dts") DO "ffmpeg.exe" -report -i "%%~A" -af "pan=stereo|FL=1.369398*FC+1.261204*BL+0.815301*FL|FR=1.369398*FC+1.261204*BR+0.815301*FR" -b:a %ac3Bit% "%outputDir%\%%~nA.ac3"
goto end
:dk7
FOR %%A IN ("%sourceDir%\*.dts") DO "ffmpeg.exe" -report -i "%%~A" -c libvo_aacenc -af "pan=stereo|FL=1.369398*FC+1.261204*BL+0.815301*FL|FR=1.369398*FC+1.261204*BR+0.815301*FR" -b:a %m4aBit% "%outputDir%\%%~nA.m4a"
FOR %%A IN ("%sourceDir%\*.dts") DO "ffmpeg.exe" -report -i "%%~A" -af "pan=stereo|FL=1.369398*FC+1.261204*BL+0.815301*FL|FR=1.369398*FC+1.261204*BR+0.815301*FR" -b:a %ac3Bit% "%outputDir%\%%~nA.ac3"
goto end
:log0
del *.log
goto end
:end
ECHO.
ECHO All done!
ECHO.