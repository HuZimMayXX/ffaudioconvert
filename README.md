**Introduction**\
Use ffAudioConvert20.bat to convert Stereo audiofiles into other formats (m4a, mp3, ac3, ogg).\
Use ffAudioConvert51.bat to convert 5.1 audiofiles into proper Stereo mixes (m4a or ac3).\
Each file has the option to correct PAL SpeedUp if present. But if chosen, it does this for all the files in the directory.\
Start the respective file to see your choices, as they're self-explanatory.\
You can copy these batchfiles freely as you wish, in any shape or form, with no credit needed (but appreciated).\
\
**Editing the bat**\
You can edit the bat to set your directories, which is highly recommended, since you might not have everything in the same directories as I have.\
You can also edit the bitrates for each parent-option (m4a, mp3, ac3, ogg), since you might not want the same bitrates I use (76, 128, 192, 128 respectively). I suggest not editing the ac3 bitrate, but you do you.\
\
**Note regarding DTS**\
Sometimes ffmpeg seems to have problems distinguishing the front and back channels in DTS files. I've added an F (for front) and B (for back) at the end of the respective options, in case the wrong channels got amplified.\
\
**Why did I do this?**\
I don't have a 5.1 system and really dislike how loud the sound can get, especially when its followed by really quiet segements in some movies/series. You sometimes also have the problem of PAL SpeedUps, where the sound just isn't right. I'm from Germany, so with the start of DVDs getting released, I've noticed they tend to use PAL SpeedUp to have the video run at PAL Speed - including all problems which come with it. This was extremely irritating, as the German dub for example was also sped up (it was dubbed and broadcast in the original speed-  a careful PAL conversion), and nowadays TV channels either broadcast in SpeedUp or (even worse) dub the sped up footage, thanks to complains from DVD owners - (which in turn sounds really bad on Blu-Ray). Anyway, I wanted to do something against the former ("dub in in original speed and broadcast it in SpeedUp), so a batch file was needed so I don't have to enter the command line every single time. I also added m4a (since I use audiofiles in this format as a sleeping help) and my "usb bitrate" to make it more convenient for me. And while I did this only for myself, I hope other people might find it handy as well.